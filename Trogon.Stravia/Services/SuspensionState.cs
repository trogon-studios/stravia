using System;

namespace Trogon.Stravia.Services
{
    public class SuspensionState
    {
        public Object Data { get; set; }
        public DateTime SuspensionDate { get; set; }
    }
}
