using Trogon.Stravia.ViewModels;

using Windows.UI.Xaml.Controls;

namespace Trogon.Stravia.Views
{
    public sealed partial class MainPage : Page
    {
        public MainViewModel ViewModel { get; } = new MainViewModel();
        public MainPage()
        {
            InitializeComponent();
        }
    }
}
